# Assignment4: Heads First Servlets and JSP
## Chapter 3: Demostration Code

This repository contains updated sample code from the book.

First install Tomcat 9.0.58 (or any later version in the 9.0 series).

You'll need to create a new run configuration in IntelliJ

1. Select Run -> Edit Configurations
2. Click "Add new configuration"
3. Select "Tomcat Server - Local"
4. On the Deployment tab, click on the "+" and select "Artifact".  Use the exploded war option.
5. On the server tab, click on the "Configure ..." button next to "Application server:".  Set the Tomcat home directory based upon where you previously installed Tomcat.  Click Ok
5. On the server tab, add "Assignment4HeadsFirstJSP_war_exploded/" to the end of the URL if it is not already there.
6. Optionally, Change the Name at the of the dialog box.
7. Click "ok".  Now run that configuration
